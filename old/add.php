<?php $head_title = 'Add new contact'; ?>
<?php require_once "components/header.php"; ?>
<p><a href="/">Вернуться на главную</a></p>

<form action="/add-contact.php" method="POST">
  <div class="form-item">
    <label for="contact_name">Введите имя</label>
    <input type="text" name="contact_name">
  </div>
  <div class="form-item">
    <label for="contact_phone">Введите Телефон</label>
    <input type="text" name="contact_phone">
  </div>
  <div class="form-item form-action">
    <input type="submit" class="form-submit" value="Добавить">
  </div>
</form>

<?php require_once "components/footer.php"; ?>
