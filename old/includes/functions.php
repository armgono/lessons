<?php
 /**
  * Add new contact to global addressbook
  * @param string name
  *   contact name
  * @param string phone
  *   contact phone number
  */
 function addContact($name, $phone){
  // Создаем объект контакта
  $contact = new stdClass; // Create empty object
  $contact->name = $name;
  $contact->phone = $phone;
  // Добавляем в справочник новый контакт
  global $addressbook;
  $addressbook[] = $contact;
}


/**
 * Search contact by name
 */

function findContact($name){
  global $addressbook;
  foreach($addressbook as $contact){
    if($contact->name == $name){
      return $contact;
    }
  }
  return FALSE;
}
