<?php
/**
 * @file
 *   содержит функции для работы с базой данных
 */

/**
 * Выполняет подключение к базе данных
 * сохраняет ссылку(PDO объект) на базу в глобальной переменнной $db
 */
function connect(){
  // Данные для подключения к базе
  $db_host = 'localhost';
  $db_port = '33067';
  $db_name = 'addressbook';
  $db_user = 'vard';
  $db_pass = 'parol';
  $db_charset = 'utf8';

  // Строка подключения
  $dsn = 'mysql:host=' . $db_host . ';port=' . $db_port . ';dbname=' . $db_name . ';charset=' . $db_charset;


  // Проверяем подключение к базе, если не получилось, выводим сообщение об ошибке
  // Если подключились успешно, сохраняем ссылку на объект базы в переменной $db
  global $db;
  try {
    $db = new PDO($dsn, $db_user, $db_pass);
  } catch (PDOException $e) {
    die('Подключение не удалось: ' . $e->getMessage());
  }

}


/**
 * Добавляет новый контакт в базу
 * @param string $name
 *   Имя контакта
 * @param string $phone
 *   Номер телефона контакта
 */
function addData($name, $phone){
  global $db;
  $table = 'contacts';
  $db->query("INSERT INTO $table (id, name, phone) VALUES (NULL, '$name', '$phone');");
}

/**
 * Получает из базы список всех контаков
 * @return array
 * Массив контаков, каждый контакт является объектом
 */
function getData(){
  global $db;
  $table = 'contacts';
  $data = $db->query("SELECT * FROM $table")->fetchAll(PDO::FETCH_CLASS);
  return $data;
}


/**
 * Удаляет указанный контакт из базы
 * @param int
 *   $id контакта, которй нужно удалить
 */
function delContact($id){
  global $db;
  $table = 'contacts';
  $db->query("DELETE FROM $table WHERE id=$id");
}


/**
 * Загружает 1 контакт по ID из базы
 * @param int $id
 *   id контакта, которй нужно загрузить
 * @return object $contact
 *   Контакт соответствующий полученному ID
 */
function getContact($id){
  global $db;
  $table = 'contacts';
  $contact = $db->query("SELECT * FROM $table WHERE id=$id")->fetchObject();
  return $contact;
}


/**
 * Обновляет контакт в базе
 * @param int $id
 *   iD контакта
 * @param string $name
 *   Имя контакта
 * @param string $phone
 *   Номер телефона контакта
 */
function updateData($id, $name, $phone){
 global $db;
 $table = 'contacts';
 $db->query("UPDATE $table SET name='$name', phone='$phone' WHERE id=$id");
}
