<?php
function showContacts($contacts){
  $out = '<table cellpadding="0" cellspacing="0">';
  $out .= '<tr>';
  $out .= '<th>Name</th>';
  $out .= '<th>Phone</th>';
  $out .= '<th>Actions</th>';
  $out .= '</tr>';
  foreach($contacts as $contact){
    $out .= '<tr>';
    $out .= '<td>' . $contact->name . '</td>';
    $out .= '<td>' . $contact->phone .'</td>';
    $out .= '<td><div class="actions"><a href="/edit.php?id=' . $contact->id . '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="/del.php?id=' . $contact->id . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></td>';
    $out .= '</tr>';
  }
  $out .= '</table>';
  return $out;
}

function showLinks(){
  $out  = '<a href="/add.php">Добавить контакт</a>';
  return $out;
}
