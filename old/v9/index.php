<?php

require_once '../src/functions.php';

$text = 'barev Varduhi, vonc es?';
debugVar('text: ' . $text);

$shifr = shifr($text, 'tU1');
debugVar('shifr: ' . $shifr);

$newText = unshifr($shifr, 'tU1');
debugVar('newText: ' . $newText);

if ($text == $newText) {
  print 'Ok';
}
else {
  print 'No';
}

function shifr($str, $sold = 'xXx') {
  $shifr = '';
  $str = str_replace(' ', $sold, $str);
  $length = strlen($str);
  if ($length % 2) {
    $shifr .= substr($str, -1);
    $str = substr($str, 0, $length - 1);
    $length--;
  }
  for ($i = 0; $i < $length / 2; $i++) {
    $chars = substr($str, $i * 2, 2);
    $shifr .= strrev($chars);
  }
  return $shifr;
}

function unshifr($shifr, $sold = 'xXx') {
  $str = '';
  $length = strlen($shifr);

  if ($length % 2) {
    $lastLetter = substr($shifr, 0, 1);
    $shifr = substr($shifr, 1);
    $length--;
  }
  else {
    $lastLetter = '';
  }
  for ($i = 0; $i < $length / 2; $i++) {
    $chars = substr($shifr, $i * 2, 2);
    $str .= strrev($chars);
  }
  $str.= $lastLetter;
  $str = str_replace($sold, ' ', $str);
  return $str;
}
