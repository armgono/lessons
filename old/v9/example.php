<?php

require_once '../src/functions.php';

$text = 'barev';
debugVar('text: ' . $text);

$shifr = shifr($text);
debugVar('shifr: ' . $shifr);

$newText = unshifr($shifr);
debugVar('newText: ' . $newText);

if ($text == $newText) {
  print 'Ok';
}
else {
  print 'No';
}

function shifr($str) {
  $shifr = 'a' . $str . 'a';
  return $shifr;
}

function unshifr($shifr) {
  $str = substr($shifr, 1, strlen($shifr) - 2);
  return $str;
}

