<?php
if(empty($_GET['id'])){
  die('Page not found');
}

require_once 'includes/db.php';
$id = $_GET['id'];
connect();

$contact = getContact($id);

if(!$contact){
  header('Location: /');
}

$head_title = 'Edit contact';
?>
<?php require_once "components/header.php"; ?>
<p><a href="/">Вернуться на главную</a></p>

<form action="/update-contact.php" method="POST">
  <div class="form-item">
    <label for="contact_name">Введите имя</label>
    <input type="text" name="contact_name" value="<?php print $contact->name; ?>">
  </div>
  <div class="form-item">
    <label for="contact_phone">Введите Телефон</label>
    <input type="text" name="contact_phone" value="<?php print $contact->phone; ?>">
  </div>
  <input type="hidden" value="<?php print $contact->id; ?>" name="id">
  <div class="form-item form-action">
    <input type="submit" class="form-submit" value="Сохранить">
  </div>
</form>

<?php require_once "components/footer.php"; ?>
