<?php

/**
 * @file
 * Обрабатывает данные полученные из формы добавления контактов в базу
 */
// Проверяем цельность полученных данных из формы
if (empty($_POST['contact_name']) || empty($_POST['contact_phone'])) {
  die('Page not found');
}
// Сохраняем полученные данные в переменные
$name = $_POST['contact_name'];
$phone = $_POST['contact_phone'];

// Подключаем функционал базы данных
require_once 'includes/db.php';

connect();
addData($name, $phone);
header('Location: /');
