<?php

header('Content-Type: text/html; charset=utf-8');

require_once '../src/functions.php';

$n = 4;

function sumN($number) {
    if ($number > 1) {
    return $number + sumN($number - 1);
  }
  return $number;
}

//print sumN($n);

/**
 *  numebr = 4
 *  4 + 3 + 2 + 1 =
 */
$autos = array(
  array(
    'title' => 'грузовые',
    'childs' => array(
      array('title' => 'Maz'),
      array('title' => 'Kraz'),
      array('title' => 'KAMAZ'),
      array('title' => 'ZIL'),
    )
  ),
  array(
    'title' => 'легковые',
    'childs' => array(
      array('title' => 'Vaz'),
      array(
        'title' => 'Volkswagen',
        'childs' => array(
          array('title' => 'audi'),
          array('title' => 'Bmw'),
          array('title' => 'Opel'),
        )
      ),
      array('title' => 'Mercedes'),
    ),
  ),
);

function returnTree($arr, $prefix = '') {
  $out = '';
  foreach ($arr as $item) {
    $out .= $prefix . $item['title'] . '<br>';
    if (isset($item['childs'])) {
      $out .= returnTree($item['childs'], $prefix . '---');
    }
  }
  return $out;
}

print returnTree($autos);


/// НА чтение http://www.php.su/functions/custom/?3