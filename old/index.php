*//<?php
require_once 'includes/db.php';
require_once 'includes/main.php';
require_once 'includes/view.php';

$pdo = NULL;
init();
$contacts = getData();
$head_title = 'Contacts';

require_once "components/header.php";

print showContacts($contacts);
print showLinks();
require_once "components/footer.php";

