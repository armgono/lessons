<?php
if(empty($_POST['contact_name']) || empty($_POST['contact_phone']) || empty($_POST['id'])){
  die('Page not found');
}

$name = $_POST['contact_name'];
$phone = $_POST['contact_phone'];
$id = $_POST['id'];

require_once 'includes/db.php';

connect();
updateData($id, $name, $phone);
header('Location: /');
