<?php
 /*
  Addressbook is array of adresses,
   every address is keyed array contain:
   string name - contact name
   string phone - contact phone number
  */


 $addressbook = array();

 /**
  * Add new contact to global addressbook
  * @param string name
  *   contact name
  * @param string phone
  *   contact phone number
  */
 function addContact($name, $phone){
  global $addressbook;
  $addressbook[] = array(
    'name' => $name,
    'phone' => $phone,
    );
 }

print '<pre>';print_r($addressbook);
addContact('Varduhi', '+7 (938) 551-55-70');
print '<pre>';print_r($addressbook);
addContact('Gevorg', '+7 (938) 527-80-35');
print '<pre>';print_r($addressbook);
addContact('Gevorg2', '+7 (961) 001-02-03');
print '<pre>';print_r($addressbook);

?>
<!DOCTYPE html>
<html>
<head>
  <title>AddressBook</title>
  <style>
    table{
      width: 400px;
    }
    th, td{
      padding: 5px 10px;
      border: 1px solid;
    }
  </style>
</head>
<body>
  <table cellpadding="0" cellspacing="0">
    <tr>
      <th>Name</th>
      <th>Phone</th>
    </tr>
    <?php
      // print each contact from addressbook
    foreach($addressbook as $contact){
        print '<tr>'; // New row
          print '<td>' . $contact['name'] . '</td>'; // Name col
          print '<td>' . $contact['phone'] . '</td>'; // Phone col
        print '</tr>'; // End new row
      }
      ?>
    </table>

  </body>
  </html>

