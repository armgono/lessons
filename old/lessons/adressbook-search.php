<?php
 /*
  Addressbook is array of adresses,
   every address is object contain:
   string name - contact name
   string phone - contact phone number
  */

   $addressbook = array();

 /**
  * Add new contact to global addressbook
  * @param string name
  *   contact name
  * @param string phone
  *   contact phone number
  */
 function addContact($name, $phone){
  // Создаем объект контакта
  $contact = new stdClass; // Create empty object
  $contact->name = $name;
  $contact->phone = $phone;
  // Добавляем в справочник новый контакт
  global $addressbook;
  $addressbook[] = $contact;
}

function findContact($name){
  global $addressbook;
  foreach($addressbook as $contact){
    if($contact->name == $name){
      return $contact;
    }
  }
  return FALSE;
}

addContact('Varduhi', '+7 (938) 551-55-70');
addContact('Gevorg', '+7 (938) 527-80-35');
addContact('Gevorg2', '+7 (961) 001-02-03');

if(!empty($_GET['name'])){
  $name = $_GET['name'];
  $contact = findContact($_GET['name']);
}


?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>AddressBook</title>
  <style>
    table{
      width: 400px;
    }
    th, td{
      padding: 5px 10px;
      border: 1px solid;
    }
  </style>
</head>
<body>
  <form action="/adressbook-search.php" method="GET">
    <label for="name">Введите имя для поиска</label> <input type="text" name="name" value="<?php print (empty($name) ? '' : $name); ?>">  <input value="Отправить" type="submit">
  </form>
  <br><hr>

  <?php if(isset($contact)):?>
    <?php if(empty($contact)): ?>
      <p>Не найдены контакты с именем "<?php print $name; ?>".</p>
    <?php else: ?>

      <table cellpadding="0" cellspacing="0">
        <tr>
          <th>Name</th>
          <th>Phone</th>
        </tr>
        <tr>
          <td><?php print $contact->name; ?></td>
          <td><?php print $contact->phone; ?></td>
        </tr>
    </table>
  <?php endif;?>
<?php endif; ?>
</body>
</html>

