<?php
 require_once 'includes/functions.php';
 require_once 'includes/exampledata.php';
 require_once 'includes/main.php';
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>AddressBook</title>
  <style>
    table{
      width: 400px;
    }
    th, td{
      padding: 5px 10px;
      border: 1px solid;
    }
  </style>
</head>
<body>
  <form action="/" method="GET">
    <label for="name">Введите имя для поиска</label> <input type="text" name="name" value="<?php print (empty($name) ? '' : $name); ?>">  <input value="Отправить" type="submit">
  </form>
  <br><hr>
  <?php if(isset($contact)):?>
    <?php if(empty($contact)): ?>
      <p>Не найдены контакты с именем "<?php print $name; ?>".</p>
    <?php else: ?>
      <table cellpadding="0" cellspacing="0">
        <tr>
          <th>Name</th>
          <th>Phone</th>
        </tr>
        <tr>
          <td><?php print $contact->name; ?></td>
          <td><?php print $contact->phone; ?></td>
        </tr>
      </table>
    <?php endif;?>
  <?php endif; ?>
</body>
</html>


