<?php

if(!empty($_GET['name'])){
  $name = $_GET['name'];
  print 'Hello ' . $name;
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Test form</title>
</head>
<body>
<form action="/" method="GET">
  <label for="name">Ваше имя</label> <input type="text" name="name" value="<?php print isset($name)? $name:''; ?>"><br>
  <input name="send" value="Отправить" type="submit">
</form>
<div>
 <p><b>$_GET</b>: СУПЕРГЛОБАЛЬНЫЙ массив содержит гет параметры</p>
 <p><b>isset</b>: TRUE если переменная существует, FALSE если не определена</p>
 <p><b>empty</b>: TRUE если переменная не существует или существует и имеет пустое значение, FALSE если переменная имеет не пустое значение</p>
</div>
</body>
</html>
