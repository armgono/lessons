<?php
if (!empty($_POST)) {
  require_once './functions.php';
  $key = $_POST['key'];
  $text = $_POST['text'];
  if ($_POST['action']) {
    $message = unshifr($text, $key);
  }
  else {
    $message = shifr($text, $key);
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Шифровальщик</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <form action="/v10/index.php" method="POST">
      <label for="text">Введите текст</label>
      <textarea name="text" required="required"></textarea>
    </div>
    <div class="form-item">
      <label for="key">Введите ключ</label>
      <input type="text" name="key"  required="required">
    </div>
    <div class="form-item">
      <label for="action">Действие</label>
      <select name="action">
        <option value="0" selected>Шифровать</option>
        <option value="1">Расшифровать</option>
      </select>
    </div>
    <div class="form-item form-action">
      <input type="submit" class="form-submit" value="Выполнить">
    </div>
  </form>
  <?php if (isset($message)): ?>
    <textarea rows=10 cols=40 style="padding: 10px"><?php print $message; ?></textarea>
  <?php endif; ?>
</body>
</html>