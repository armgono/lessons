<?php

/**
 * @file
 * содержит функции для шифрования текстов 
  /**
 * Выполняет шифрование текста
 */
function shifr($str, $sold = 'xXx') {
  $shifr = '';
  //Заменяет пробелы данным ключом
  $str = str_replace(' ', $sold, $str);
  $length = strlen($str);
  // если длина строки нечетная, то последняя буква становится первым символом 
  // шифра 
  if ($length % 2) {
    $shifr .= substr($str, -1);
    $str = substr($str, 0, $length - 1);
    $length--;
  }
  //меняет пары букв местами поочередно
  for ($i = 0; $i < $length / 2; $i++) {
    $chars = substr($str, $i * 2, 2);
    $shifr .= strrev($chars);
  }
  return $shifr;
}
//Выполняет дешифрование текста
function unshifr($shifr, $sold = 'xXx') {
  $str = '';
  $length = strlen($shifr);
// Если длина строки нечетная, то первый символ становится последней буквой
  if ($length % 2) {
    $lastLetter = substr($shifr, 0, 1);
    $shifr = substr($shifr, 1);
    $length--;
  }
  else {
    $lastLetter = '';
  }
  //Меняет пары букв местами поочередно
  for ($i = 0; $i < $length / 2; $i++) {
    $chars = substr($shifr, $i * 2, 2);
    $str .= strrev($chars);
  }
  //Заменяет пробелы данным ключом
  $str.= $lastLetter;
  $str = str_replace($sold, ' ', $str);
  return $str;
}
