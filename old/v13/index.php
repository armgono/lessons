<?php

require_once '../src/functions.php';
$str = 'Barev Varduhi, vonc es? inch ka chka?';
$key = 10;
$shifr = cesar($str, $key);
debugVar($shifr);

for ($i = 1; $i < 61; $i++) {
  print $i . ': ' . cesar($shifr, $i) . "<br>";
  print -$i . ': ' .cesar($shifr, -$i) . "<br>";
}

function cesar($str, $key) {
  $alphabet = 'abcdefghijklmnopqrstuvwxyz';
  $alphabet .=' ,!.?:+-_' . strtoupper($alphabet);
  $length = strlen($alphabet); // 52
  if ($key >= $length) {
    $key %= $length;
  }
  if ($key == 0) {
    $result = $str;
    return $result;
  }
  $result = '';
  $chars = preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);
  foreach ($chars as $char) {
    $position = strpos($alphabet, $char);
    if ($position !== FALSE) {
      $newPos = $position + $key;
      if ($newPos >= $length) {
        $newPos -= $length;
      }
      $result .= substr($alphabet, $newPos, 1);
    }
    else {
      $result .= $char;
    }
  }
  return $result;
}

function uncesar($str, $key) {
  return cesar($str, -$key);
}