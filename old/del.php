<?php
if(empty($_GET['id'])){
  die('Page not found');
}

$id = $_GET['id'];
require_once 'includes/db.php';

connect();
delContact($id);

header('Location: /');
