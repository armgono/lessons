<pre>
  <?php
  $nums = array(9, 4, 19, -34, 45, 667, 898);

  function isEven($num) {
    return $num % 2 == 0;
  }

  function isOdd($num) {
    return !isEven($num);
  }

  function positive($num) {
    return $num > 0;
  }

  $min = min($nums);
  $max = max($nums);
  $zuygMax = max(array_filter($nums, 'isEven'));
  $kentMin = min(array_filter($nums, 'isOdd'));

  $positives = array_filter($nums, 'positive');
  print_r($positives);
  print "min: $min\n "
      . "max: $max\n"
      . "zuygMax: $zuygMax\n"
      . "kentMin: $kentMin";
  ?>
</pre>



