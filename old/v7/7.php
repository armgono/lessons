<pre>
  <?php
  $nums = array(9, 4, 19, -34, 45, 667, 898);
  $min = reset($nums);
  $max = $min;
  foreach ($nums as $number) {
    if ($number < $min) {
      $min = $number;
    }
    elseif ($number > $max) {
      $max = $number;
    }
    if ($number % 2) {
      if (!isset($kentMin)) {
        $kentMin = $number;
      }
      elseif ($number < $kentMin) {
        $kentMin = $number;
      }
    }
    else {
      if (!isset($zuygMax)) {
        $zuygMax = $number;
      }
      elseif ($number > $zuygMax) {
        $zuygMax = $number;
      }
    }
  }
  if (!isset($kentMin)) {
    $kentMin = 'No odd item';
  }
  if (!isset($zuygMax)) {
    $zuygMax = 'No even item';
  }
  print "min: $min\n "
      . "max: $max\n"
      . "zuygMax: $zuygMax\n"
      . "kentMin: $kentMin";
  ?>
</pre>



