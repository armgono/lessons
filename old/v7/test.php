<?php
print '0 == FALSE: ';
var_dump(0 == FALSE);

print "<br>";
print '123 == FALSE: ';
var_dump(123 == FALSE);

print "<br>";
print '123 == TRUE: ';
var_dump(123 == TRUE);

print "<br>";
print '"0" == FALSE: ';
var_dump("0" == FALSE);