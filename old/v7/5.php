<pre>
<?php
$nums = array(9, 4, 19, -34, 45, 667, 898);
function calculateMin($array) {
  $min = reset($array);
  foreach ($array as $number) {
    if ($number < $min) {
      $min = $number;
    } 
  }
  return $min;
}

print calculateMin($nums);
print "\n";

function calculateMax($array) {
  $min = reset($array);
  foreach ($array as $number) {
    if ($number > $min) {
      $max = $number;
    } 
  }
  return $max;
}
print calculateMax($nums);
?>
</pre>