<pre>
  <?php
// Дан массив
  $nums = array(9, 4, 19, -34, 45, 667, 898);
  /**
   * 1. Найти минимальное значение в массиве
   * 2. Найти максимальное значение в массиве
   * 3. Найти минимальное нечетное значение в массиве
   * 4. Найти максимальное четное значение в массиве       
   * 5. Написать функции упрощающие решения задач 
   * 6. (опц) Написать без функции но выполнить рассчет всех 4-х задач в 1 цикле. 
   * 7. (опц) Реализовать тоже самое, используя встроенные функции PHP
   */
  if(empty($nums)){
    print 'empty array';
    exit;
  }
  $min = reset($nums);
  foreach ($nums as $num) {
    if ($num < $min) {
      $min = $num;
    }
  }
  print $min;
  print "\n";

  $max = reset($nums);
  foreach ($nums as $value) {
    if ($value > $max) {
      $max = $value;
    }
  }
  print $max;
  print "\n";


  $kent = array();
  $zuyg = array();
  foreach ($nums as $num) {
    if ($num % 2) {
      $kent[] = $num;
    }
    else {
      $zuyg[] = $num;
    }
  }
  if (empty($kent)) {
    print 'No odd items';
  }
  else {
    $minKent = reset($kent);
    foreach ($kent as $knum) {
      if ($knum < $minKent) {
        $minKent = $knum;
      }
    }
    print $minKent;
  }
  print "\n";

  $maxZuyg = reset($zuyg);
  foreach ($zuyg as $znum) {
    if ($znum > $maxZuyg) {
      $maxZuyg = $znum;
    }
  }
  print $maxZuyg;
  ?>

</pre>
