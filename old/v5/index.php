<?php

//$str = 'Hello world';
//$suffix = '!';
//$text = $str . $suffix;
//print $text;
//print "\n";
//
//$length = strlen($text);
//print $length;
//print "\n";
//
//$world = substr($text, 6);
//print $world;
//print "\n";
//
//$world = substr($text, 6, 5);
//print $world;
//print "\n";
//
//$newText = substr($text, 1);
//print $newText;
//print "\n";
//
//
//$noL = str_replace('l', 'p', $text);
//print $noL;
//print "\n";
//
//$pattern = array('el', 'or');
//$replace = array('il', 'ar');
//$changed = str_replace($pattern, $replace, $text);
//print $changed;
//print "\n";
//
//$text = 'Hello [name]. How are you';
//$name = 'Varduhi';
//
//print str_replace('[name]', $name, $text);
//print "\n";

$name = 'Ashot';
$surname = 'Kaputyan';
$phone = '+7123';
$message = 'Hello my friend';


$mailBody = "Submitted form from site, by user: [name]\n"
    . "Name: [name]\n"
    . "Surname: [surname]\n"
    . "tel: [phone]\n"
    . "text: [text]\n";

$tokens = array(
  '[name]' => $name,
  '[surname]' => $surname,
  '[phone]' => $phone,
  '[text]' => $message,
);

$mailBodyReady = applyTokens($mailBody, $tokens);

print $mailBodyReady;
print "\n";

$welcomeText = "Hello: {name}\n"
    . "Welcom to our site."
    . "Today is !date";

$welcomeTokens = array(
  '{name}' => $name,
  '!date' => date('d.m.Y'),
);

print applyTokens($welcomeText, $welcomeTokens);

function applyTokens($template, $tokens) {
  $pattern = array_keys($tokens);
  $replace = array_values($tokens);
  return str_replace($pattern, $replace, $template);
}
