<!DOCTYPE html>
<html>
  <head>
    <title><?php print $page_title; ?></title>
    <link rel="stylesheet" href="/css/reset.css"/>
    <link rel="stylesheet" href="/css/style.css"/>
  </head>
  <body>
    <div id="wrapper">
      <div id="header">
        <div id="logo">
          <img src="/images/logo.png">
        </div>
        <div id="menu">
          <ul class="menu">
            <li><a href="index.php"<?php if($current_page == 'front') print ' class="active"';?>>Home</a></li>
            <li><a href="contacts.php"<?php if($current_page == 'contacts') print ' class="active"';?>>Contacts</a></li>
          </ul>
        </div>
      </div>
